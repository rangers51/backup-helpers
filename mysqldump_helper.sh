#!/bin/sh

# usage example: ./mysqldump_helper.sh "database1 database2 database3"

cd /usr/local/bin

# List of databases to be backed up separated by space, passed in as argument.
dblist=$1

# Directory for backups
# this will be the temp folder on the server to store the files until they're done FTPing.
backupdir=/home/mysqlbackups
 
# Number of versions to keep
numversions=1
 
# Full path for MySQL dump command
mysqldumpcmd=/bin/mysqldump
 
# MySQL config string.
# example: " --user=mysqluser -h 127.0.0.1 "
configstring="CONFIGSTRING"
 
# Create directory if needed
mkdir -p ${backupdir}
if [ ! -d ${backupdir} ] 
then
    echo "Invalid directory: ${backupdir}"
exit 1
fi

#FTP bits
HOST='HOSTNAME'
PORT='PORT'
USER='USERNAME'
PASSWD='PASSWORD'

# Hotcopy begins here
echo "Dumping MySQL Databases..."
RC=0
for database in $dblist
do
    echo "Dumping $database ..."
    $mysqldumpcmd $configstring $database > ${backupdir}/${database}.sql
    RC=$?
    if [ $RC -gt 0 ]
    then
        break;
    fi
 
    # Rollover the backup directories
    i=$numversions
    mv ${backupdir}/${database}.sql ${backupdir}/${database}.sql.0 2> /dev/null
    rm -fr ${backupdir}/${database}.sql.$i 2> /dev/null
    while [ $i -gt 0 ]
    do
         mv ${backupdir}/${database}.sql.`expr $i - 1` ${backupdir}/${database}.sql.$i 2> /dev/null
         i=`expr $i - 1`
    done
    #TAR and GZIP the backup
    #first get the date of the backup and assign it a name.
    FIL="${database}_backup.`date '+%Y%m%d_%H%M'`.tar.gz"
    tar -zcvf ${backupdir}/${database}.tar.gz ${backupdir}/${database}.sql.1
    cd ${backupdir}
    mv ${database}.tar.gz $FIL
    #script for FTP - this should push each database file to its proper folder - the folders should exist on the remote FTP box going in.
    temp="/tmp/$(basename $0).$$" ; trap "/bin/rm -f $temp" 0
    echo "quote user $USER" > $temp
    echo "quote pass $PASSWD" >> $temp
    echo "binary" >> $temp
#   if you need to transfer passively, uncomment the next line.    
#   echo "passive" >> $temp
    echo "lcd ${backupdir}" >> $temp
    echo "cd ${database}" >> $temp
    echo "put $FIL" >> $temp
    echo "cd ../" >> $temp
    echo "quit" >> $temp

    ftp -n $HOST $PORT < $temp
done
 
if [ $RC -gt 0 ]
then
    echo "MySQL Dump failed!"
    exit $RC 
else
    # Hotcopy is complete. List the backup versions!
    ls -l ${backupdir}
    echo "MySQL Dump is complete!"
fi

# all done with the list of databases, clean up the local server.
rm $FIL
for database in $dblist
do
    rm -r ${backupdir}/${database}.sql.$numversions
done
rm -r ${backupdir}/*.tbz2
rm -r ${backupdir}/*.sql.*
rm -r ${backupdir}/*.tar.gz

exit 0
