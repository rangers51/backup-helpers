#!/bin/sh

# pkgacct script to back up full accounts using cpanel's pkgacct script.

# usage example: ./pkgacct_helper.sh account_name

cd /scripts
./pkgacct $1

cd /home/
#first get the date of the backup and assign it a name.
FIL="cpmove-${1}.`date '+%Y%m%d_%H%M'`.tbz2"
#then tar and bzip it
tar cvf - cpmove-${1}.tar.gz | bzip2 -7 > ${1}.tbz2
mv ${1}.tbz2 $FIL

#FTP bits
HOST='HOST'
PORT='PORT'
USER='USER'
PASSWD='PASSWORD'

#script
temp="/tmp/$(basename $0).$$" ; trap "/bin/rm -f $temp" 0
echo "quote user $USER" > $temp
echo "quote pass $PASSWD" >> $temp
echo "binary" >> $temp
# uncomment this line if you need passive transfer.
# echo "passive" >> $temp
echo "put $FIL" >> $temp
echo "quit" >> $temp

ftp -n $HOST $PORT < $temp

# clean up on the source server.
rm $FIL
rm cpmove-${1}.tar.gz

exit 0
