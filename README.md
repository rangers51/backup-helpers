# README #

### What is this repository for? ###

* Helper bash scripts to easily back up MySQL databases or entire CPanel accounts and FTP the results to a server of your choice. Intended for WHM/CPanel users. 
* Version 1.0 (February 2017), based from original bespoke scripts in use since 2010
* Can be used from CLI or in crontab for automation

### Running the scripts ###

* Scripts should be placed in a location that makes sense, either already in or added to $PATH for ease of use.
* Usage examples are in each script.
* File and user permissions should be checked in case of issue.

### Who do I talk to? ###

* Josh Alvies (@rangersfiftyone)

### Credits ###

* The original script leveraged the deprecated mysqlhotcopy, and some small parts of this script were certainly borrowed from other things that were out on the internet seven or eight years ago. I can't locate any likely sources at the moment, so if any of this looks like something you might have worked on in the past, please let me know!